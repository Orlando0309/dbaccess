/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package date;

/**
 *
 * @author orlando
 */
public class Month {
    String nom;
    Integer number;

    public Month(String nom, Integer number) {
        this.nom = nom;
        this.number = number;
    }
    
    @Override
    public String toString() {
        return nom;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
    
}
