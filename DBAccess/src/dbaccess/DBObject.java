/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbaccess;

import doframe.own.annotation.DynamicAnnot;
import doframe.own.generic.dao.Genericdao;
import doframe.own.generic.dao.Query;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import terminal.Arithmetic;
import terminal.Terminal;

/**
 *
 * @author orlando
 * @param <T>
 * @param <E>
 */
public abstract class DBObject<T extends DBObject,E extends Object> {

    Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public DBObject() {}

    public DBObject(Connection con) {
        this.connection = con;
    }
    

    public void save(Connection con)
            throws Exception {
        Genericdao.save(this, (con == null) ? this.connection : con);
    }
    
    public String saveQuery(Connection con)
            throws Exception {
        return Query.save(this, (con == null) ? this.connection : con);
    }

     T[] toT(int size){
        return (T[]) Array.newInstance(this.getClass(), size);
    }
    public T[] getlist(Connection con,boolean id) 
            throws Exception
         {
        ArrayList<Object> temp = Genericdao.get(this, id, (con == null) ? this.connection : con);
        T[] retour= toT(temp.size());
        for(int i=0;i<temp.size();i++) retour[i]=(T) temp.get(i);
        return retour;
        
    }

    public T[] find(Connection con,boolean id) throws Exception
            {
        ArrayList<Object> temp = Genericdao.get(this, id, (con == null) ? this.connection : con);
        T[] retour= toT(temp.size());
        for(int i=0;i<temp.size();i++) retour[i]=(T) temp.get(i);
       return retour;
    }
//    
    public T findById(Connection con,E id) throws Exception{
        
        return (T)Genericdao.findById(this, id, ((con == null) ? this.connection : con));
    }
    
    public void update(Connection con,DBObject obj,boolean id) throws Exception{
        Genericdao.update(this, obj, ((con == null) ? this.connection : con),id);
    }
    public String updateQuery(Connection con,DBObject obj,boolean id) throws Exception{
        return Query.update(this, obj, ((con == null) ? this.connection : con),id);
    }
    public void delete(Connection con,boolean id) throws Exception{
        Genericdao.delete(this, ((con == null) ? this.connection : con),id);
    }
    public String deleteQuery(Connection con,boolean id) throws Exception{
        return Query.delete(this, ((con == null) ? this.connection : con),id);
    }
    public  boolean executeQuery(String query,Connection con) throws SQLException{
        return Query.executeQuery(query, ((con == null) ? this.connection : con));
    }
    
    public E saveGetId(Connection con) throws Exception{
            Genericdao.save(this, (con == null) ? this.connection : con);
          T[] liste=getlist(con, true);
          Arithmetic<T> arithm=new Arithmetic();
          arithm.setObject(liste);
          Field PK=DynamicAnnot.getPK(this);
          arithm.order(Arithmetic.DESC,PK);
          Method met=Terminal.getters2(this,PK.getName())[0];
          return (E)met.invoke(liste[0]);
    }
    
   
}
