/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacion;

import dbaccess.DBObject;

/**
 *
 * @author orlando
 * @param <T>
 * @param <E>
 * @param <M>
 */
public abstract class Hijo<T extends Hijo,E extends Object,M extends Madre> extends DBObject<T,E>{
   M madre;
   
   
    public M getMadre() {
        return madre;
    }

    public void setMadre(M madre) {
        this.madre = madre;
    }
    public abstract void setMadreId(E a) throws Exception;



   
}
