/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacion;

import dbaccess.DBObject;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author orlando
 * @param <T>
 * @param <K>
 * @param <E>
 */
public abstract class Madre<T extends DBObject,K extends Hijo,E extends Object> extends DBObject<T,E> {

    
    K[] hijos;
    public K[] getHijos() {
        return hijos;
    }
    public void setHijos(K[] hijos)  throws Exception{
        this.hijos = hijos;
    }
    K[] toT(int size, Class<K> obj) {
        return (K[]) Array.newInstance(obj, size);
    }
    
    public void extend(Class<K> classe) throws InstantiationException, IllegalAccessException{
        boolean New=this.hijos==null;
        int alava=(New)?0:this.hijos.length;
        K vaovao= (K)classe.newInstance();
        K[] shijos;
        if(New){
             shijos=toT(1, classe);
        }else{
            shijos=Arrays.copyOf(this.getHijos(),alava+1);
        }
//        K[] shijos=(New)?new K[1]:Arrays.copyOf(this.getHijos(),alava+1);
        shijos[alava]=vaovao;
        shijos[alava].setConnection(this.getConnection());
        this.hijos=shijos;
    }
    public void addHijos(K hijo){
        int alava=this.hijos.length;
        K[] shijos=Arrays.copyOf(this.getHijos(),this.hijos.length+1);
        shijos[alava]=hijo;
        this.hijos=shijos;
    }

    /**
     *
     * @param con
     * @throws doframe.own.exception.ValueCollectorException value colelecton
     * @throws doframe.own.exception.NotableNameException tablename
     * @throws doframe.own.exception.InvalidColumnException  colonne
     * @throws doframe.own.exception.UnknownSqlException sql
     * @throws doframe.own.exception.ReflectException reflect error
     */
    @Override
    public void save(Connection con)
            throws Exception  {
            Connection connect=(con==null)?this.getConnection():con;
        try {
            connect.setAutoCommit(false);
        } catch (SQLException ex) {
            Logger.getLogger(Madre.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        try{
        
        E e=this.saveGetId(con);
            System.out.println("id="+e.toString());
        for(K k:this.getHijos()){
             System.out.println("id="+e.toString());
            k.setMadreId(e);
            System.out.println("k:-->"+k.toString());
            k.save(con);
        }
        connect.commit();
        }catch(Exception ex){
           connect.rollback();
           ex.printStackTrace();
        }
    }
}
